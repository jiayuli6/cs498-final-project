# Ollama

## Deployment
- Our site is deployed to https://frosty-wescoff-076446.netlify.app/.
- Backend is hosting on [Heroku](https://cs498-final-project-0llama.herokuapp.com/) and MySQL is hosting on Google Cloud.

## Documentation
- [db create](https://docs.google.com/document/d/1YbcTuCRTFJp2qNKVhfPyhvuOrOTqNQdc3OYLjjo5xg0/edit?pli=1)
