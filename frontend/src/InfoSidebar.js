import React, { Component } from 'react'
import "./feed.css"
import axios from "axios";

import Back from './assets/back.png'

export default class InfoSidebar extends Component {
    state = {
        num_users: null,
        password: null,
        description: null,
        name: null,
        community_id: null,
        user_id: null,
        user_public_name: null,
        user_tmp_public_name: null,
        show_password: false,
        anon_checked: false
    }

    componentDidMount() {
        this.setState({user_id: this.props.user_id});

        if(this.props.visible) {
            this.renderCommunityInfo(this.props.curr_community_id);
        }
    }

    componentWillUpdate(nextProps, nextState) {
        if(nextProps.visible !== this.props.visible || nextProps.curr_community_id !== this.props.curr_community_id) {
            const community_id = nextProps.curr_community_id === undefined ? this.props.curr_community_id : nextProps.curr_community_id
            this.renderCommunityInfo(community_id);
            this.setState({show_password: false});
        }
    }

    renderCommunityInfo(community_id) {
        const config = {
            method: 'get',
            url: `https://cs498-final-project-0llama.herokuapp.com/api/community`,
            params: {
                userID: this.props.user_id,
                communityID: community_id,
            }
        }
        axios(config)
        .then((res) => {
            const curr_community = res.data[0];
            console.log(curr_community)
            this.setState({
                num_users: curr_community.User_Count,
                password: curr_community.Password,
                description: curr_community.Description,
                name: curr_community.Name,
                community_id: community_id,
                user_public_name: curr_community.Public_Name === null || curr_community.Public_Name === "NULL" ? "anon" : curr_community.Public_Name,
                user_tmp_public_name: curr_community.Public_Name === null || curr_community.Public_Name === "NULL" ? "anon" : curr_community.Public_Name,
                anon_checked: curr_community.Public_Name === null || curr_community.Public_Name === "NULL"
            })
        })
        .catch((err) => {
            console.log(err);
        })
    }

    leaveCommunity = () => {
        console.log(this.state.user_id, this.state.community_id)
        const config = {
            method: 'delete',
            url: `https://cs498-final-project-0llama.herokuapp.com/api/mycommunities`,
            params: {
                userID: this.props.user_id,
                communityID: this.state.community_id,
            }
        }
        axios(config)
        .then((res) => {
            console.log(res.data);
            this.props.reloadPage();
        })
        .catch((err) => {
            console.log(err);
        })
    }

    setPublicName() {
        if(this.state.anon_checked) {
            const config = {
                method: 'put',
                url: `https://cs498-final-project-0llama.herokuapp.com/api/mycommunities`,
                params: {
                    userID: this.props.user_id,
                    communityID: this.state.community_id,
                    publicName: "NULL"
                }
            }
            axios(config)
            .then((res) => {
                const new_info = res.data[0];
                this.setState({
                    user_public_name: new_info.Public_Name === null || new_info.Public_Name === "NULL" ? "anon" : new_info.Public_Name,
                })
            }).catch((err) => {
                console.log(err);
            })
            return;
        }
        const config = {
            method: 'put',
            url: `https://cs498-final-project-0llama.herokuapp.com/api/mycommunities`,
            params: {
                userID: this.props.user_id,
                communityID: this.state.community_id,
                publicName: this.state.user_tmp_public_name
            }
        }
        axios(config)
        .then((res) => {
            const new_info = res.data[0];
            this.setState({
                user_public_name: new_info.Public_Name
            })
        }).catch((err) => {
            console.log(err);
        })
    }

    toggleAnon(){
        this.setState({
            anon_checked: !this.state.anon_checked
        }, () => {
            if(!this.state.anon_checked) {
                this.setState({
                    user_tmp_public_name: Math.random().toString(36).substr(2, 5)
                })
            } else {
                this.setState({
                    user_tmp_public_name: "anon"
                })
            }
        })
    }

    render() {
    let className = '';
    if (this.props.visible) {
        className += 'visible';
    } else if (!this.props.visible) {
        className += 'hidden';
    }

    if (this.props.isMobile) {
        className += ' mobile';
    }
    return (
        <div id="sidebar-div-info" className = {className}>
        { this.props.visible ? 
            <div id="sidebar-community-info-subdiv">
                <div className="flex-row">
                    <div id="back-div" className = {className} onClick = {() => this.props.goBack()}>
                        <img src = {Back} className = {className}/>
                    </div>
                    <h3>Community Info:</h3>
                </div>
                <div className="sidebar-community-info light-gray-color-background black-bottom-shadow">
                    <h3>{this.state.name}</h3>
                    <p>Number of users: {this.state.num_users}</p>
                </div>
                <div className="sidebar-community-info light-gray-color-background black-bottom-shadow">
                    <h2>Your name in community: </h2>
                        <input type="text" disabled={this.state.anon_checked} value={this.state.user_tmp_public_name} className="text-field" name="community_your_name" onChange={(e) => this.setState({user_tmp_public_name: e.target.value})}/>{' '}
                    <div className="flex-row">
                        <p>Stay anonomous?</p>
                        <input type="checkbox" checked={this.state.anon_checked} value="user_public_name_anon" name="sort-order" onClick={() => this.toggleAnon()}/>{' '}
                    </div>
                    <button className={this.state.user_tmp_public_name === this.state.user_public_name ? "hidden": "visible"} onClick={() => this.setPublicName()}>Set Name</button>
                </div>
                <div className="sidebar-community-info light-gray-color-background black-bottom-shadow">
                    <button onClick={() => this.setState({show_password: !this.state.show_password})}>{this.state.show_password ? "Hide password" : "Show password"}</button>
                    <p>{this.state.show_password ? this.state.password : new Array(10).join("*")}</p>
                </div>
                <div className="sidebar-community-info light-gray-color-background black-bottom-shadow">
                    <button onClick = {() => this.leaveCommunity()}>Leave Community</button>
                </div>
            </div>
            : 
            <div>
            </div>
        }
        </div>
    )
    }
}
