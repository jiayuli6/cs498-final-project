import React, { Component } from 'react'
import './feed.css'

import HeartOutline from "./assets/HeartOutline.png"
import HeartFilled from "./assets/HeartFilled.png"
import CommentOutline from "./assets/comment.png"
import Cog from "./assets/cog.png"
import Bars from "./assets/3bars.png"
import axios from 'axios';

export default class FeedContent extends Component {
    state = {
        sort_by_date: true,
        community_id: null,
        posts: null
    }

    componentDidMount() {
        this.setState({community_id: this.props.curr_community_id})
        if(this.props.visible) {
            this.renderMiddleContent(this.props.curr_community_id);
        }
    }

    componentWillUpdate(nextProps, nextState) {
        if(nextProps.visible !== this.props.visible || nextProps.curr_community_id !== this.props.curr_community_id) {
            const community_id = nextProps.curr_community_id === undefined ? this.props.curr_community_id : nextProps.curr_community_id
            this.setState({
                sort_by_date: true,
                community_id: community_id
            });
            
            this.renderMiddleContent(community_id);
        }
    }

    toggleSortBy(event) {
        this.setState({
            sort_by_date: event.target.value==="sort_by_date"
        }, () => this.renderMiddleContent(this.state.community_id));
    }

    renderMiddleContent(community_id) {
        /* get posts from current community */
        const config = {
            method: 'get',
            url: `https://cs498-final-project-0llama.herokuapp.com/api/posts`,
            params: {
                userID: this.props.user_id,
                communityID: community_id,
                sort: this.state.sort_by_date ? "date" : "likes"
            }
        }
        axios(config)
        .then((res) => {
            console.log(res)
            const post_divs = res.data.map((item) => {
                var cutat= item.Content.lastIndexOf(' ', 100);
                var content = item.Content;
                if (cutat != -1) {
                        content=item.Content.substring(0,cutat)+'...';
                }
                console.log(item)
                return (
                <div className="post-div black-bottom-shadow">
                  <div className="flex-row">
                    <div className="post-content">
                      <h1>{item.Title}</h1>
                      <h4>{item.Public_Name}</h4>
                      <h4>{item.Date_Created}</h4>
                      <p>{content}</p>
                    </div>
                    <div id="post-expand-div">
                      <button onClick={() => this.props.setPostExpandedOverlay(item.Post_Id)}>&#10142;</button>
                    </div>
                  </div>
                  <div className="post-action-items-div">
                      <div className="post-like-div cursor-pointer" onClick={() => this.updatePostLike(item.Post_Id)}>
                        <img src={item.If_Curr_User_Liked === 1 ? HeartFilled : HeartOutline} id="post-like-heart-img"/>
                        <p>{item.Like_Count}</p>
                      </div>
                      <div className="post-like-div">
                        <img src={CommentOutline} id="post-like-heart-img"/>
                        <p>{item.Comment_Count}</p>
                      </div>
                  </div>
                </div>)
              })
      
              this.setState({
                  posts: post_divs,
              })
        })
        .catch((err) => {
            console.log(err);
        })
    }

    updatePostLike(post_id) {
        const config = {
            method: 'get',
            url: `https://cs498-final-project-0llama.herokuapp.com/api/like`,
            params: {
                userID: this.props.user_id,
                postID: post_id
            }
        }
        axios(config)
        .then((res) => {
            console.log(res.data);
            if(res.data) {
                const config = {
                    method: 'delete',
                    url: `https://cs498-final-project-0llama.herokuapp.com/api/like`,
                    params: {
                        userID: this.props.user_id,
                        postID: post_id
                    }
                }
                axios(config)
                .then((res) => {
                    console.log(res.data);
                    this.renderMiddleContent(this.state.community_id);
                })
                .catch((err) => {
                    console.log(err);
                })
            } else {
                const config = {
                    method: 'post',
                    url: `https://cs498-final-project-0llama.herokuapp.com/api/like`,
                    params: {
                        userID: this.props.user_id,
                        postID: post_id
                    }
                }
                axios(config)
                .then((res) => {
                    console.log(res.data);
                    this.renderMiddleContent(this.state.community_id);
                })
                .catch((err) => {
                    console.log(err);
                })
            }
        })
        .catch((err) => {
            console.log(err);
        })
    }

    render() {
        let className = '';
        if (this.props.visible) {
            className += 'visible';
        } else if (!this.props.visible) {
            className += 'hidden';
        }
        return (
            <div id="feed-div">
                { this.props.visible ? 
                <div id="feed-content-div">
                    <div id = "feed-top-header">
                        <div id="feed-content-header">
                            <h1>Ollama</h1>
                        </div>
                        <div id = "settings-div" onClick = {() => this.props.setShowInfo(true)}>
                            <img src={Cog} className = {className}/>
                        </div>
                        <div id = "comms-div" onClick = {() => this.props.setShowComm(true)}>
                            <img src={Bars} className = {className}/>
                        </div>
                    </div>
                    <div id="new-post-div" className="black-bottom-shadow">
                        <div id="share-something-div" onClick={() => this.props.setNewPostOverlay(true)}>
                            <img src="https://img.icons8.com/material-outlined/384/000000/pencil--v3.png"/>
                            <h3>Share something...</h3>
                        </div>
                    </div>
                    <div id="feed-sort-by-div">
                        <h4>Posts here...</h4>
                        <div id="sort-by-options-div">
                            <h4>Sort by: </h4>
                            <formgroup tag="fieldset" id="sort-order-group">
                                <formgroup check>
                                    <label check>
                                        <input type="radio" value="sort_by_date" name="sort-order" className="radio-check" checked={this.state.sort_by_date}  onChange={ (event) => { this.toggleSortBy(event) }}/>{' '}
                                        Date
                                    </label>
                                </formgroup>
                                <formgroup check>
                                    <label check>
                                        <input type="radio" value="sort_by_popularity" name="sort-order" className="radio-check" checked={!this.state.sort_by_date} onChange={ (event) => { this.toggleSortBy(event) }}/>{' '}
                                        Popularity
                                    </label>
                                </formgroup>
                            </formgroup>
                        </div>
                    </div>
                    <div id="feed-posts-div">
                        {this.state.posts}
                    </div>
                </div> 
                : 
                <div id="feed-content">
                    <div id = "feed-top-header">
                        <div id="feed-content-header">
                            <h1>Ollama</h1>
                        </div>
                        <div id = "settings-div" onClick = {() => this.props.setShowInfo(true)}>
                            <img src={Cog} className = {className}/>
                        </div>
                        <div id = "comms-div" onClick = {() => this.props.setShowComm(true)}>
                            <img src={Bars} className = {className}/>
                        </div>
                    </div>
                    <p>select community to view posts</p>
                </div>
                }
            </div>
        )
    }
}
