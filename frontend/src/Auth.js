import React, {useEffect, useState} from "react";
import "./auth.css"
import UserPool from "./authorizer/userPool";
import { CognitoUser, AuthenticationDetails } from "amazon-cognito-identity-js";
import { useHistory } from "react-router-dom";
import axios from 'axios';


const Auth = (props) => {
    const [isVisible, setVisible] = useState(props.visible);
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const [status, setStatus] = useState("");
    var currentUserID = "";
    const history = useHistory();

    // useEffect(() => {
    //     setVisible(props.visible)
    // }, [props.visible]);

    const signUp = (event) => {
        event.preventDefault();
        UserPool.signUp(email, password, [], null, (err, data) => {
            if (err) {
                if (err.stack.includes("InvalidParameterException: Username should be an email")) {
                    setStatus("Oops! Invalid email address. Please check and enter again");
                } else if (err.stack.includes("InvalidPasswordException")) {
                    setStatus("Oops! Password needs to be at least length 8, has at least one upper and lower case letters and at least one special charaters");
                } else {
                    setStatus("error occurred, please try again later...");
                }
                return;
            }
            // props.closeOverlay()
            console.log(data);
            
            const addUser = axios.create({
                baseURL: `https://cs498-final-project-0llama.herokuapp.com/`
            });

            addUser.post("/api/user", {
                userID: data.userSub
            })
                .then(function (response) {
                    console.log(response);
                })
                .catch(function (error) {
                    console.log(error);
                });
        });
    };

    const signIn = (event) => {
        event.preventDefault();
        
        const user = new CognitoUser({
            Username: email,
            Pool: UserPool 
        });

        const authDetails = new AuthenticationDetails({
            Username: email,
            Password: password
        });

        user.authenticateUser(authDetails, {
            onSuccess: data => {
                console.log('onSuccess:', data);
                user.getUserAttributes(function(err, result) {
                    if (err) {
                        alert(err);  // this throws an error (typeerror, props cannot read null)
                        return;
                    }
                    currentUserID = result[0].getValue();
                    authenticated(currentUserID);
                });
            },

            onFailure: err => {
                console.error('onFailure:', err);
            },

            newPasswordRequired: data => {
                console.log('newPasswordRequired:', data);
            }
        });
    };

    const authenticated = (currentUserID) => {
        history.push(`/feed/${currentUserID}`);
    }

    return(
        <div id="overlay-background">
            <div id="overlay-content" className="black-bottom-shadow">
                <h1>sign in</h1>
                <div className="flex-row">
                    <label>Email</label>
                    <input type="text" id="username" defaultValue={email} onChange={(event) => setEmail(event.target.value)}/>
                </div>
                <div className="flex-row">
                    <label>Password</label>
                    <input type="password" id="password" defaultValue={password} onChange={(event) => setPassword(event.target.value)}/>
                </div>
                <div className="flex-row signin-action-div">
                    <button onClick={signIn}><p>Sign In</p></button>
                    <button onClick={signUp}><p>Create Account</p></button>
                </div>
                <div>{ status }</div>
            </div>
        </div>    
    );
}

export default Auth;