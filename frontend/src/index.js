import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router, Switch, Route, Redirect } from "react-router-dom";

import './index.css';
import Feed from "./Feed";
import Auth from "./Auth";

ReactDOM.render(
  <Router basename={process.env.PUBLIC_URL}>
    <Switch>
      <Route exact path="/">
        <Redirect to={"/auth"}/>
      </Route>
      <Route exact path="/feed">
        <Redirect to={"/auth"}/>
      </Route>
      <Route path="/feed/:userID" render={(props) => (
        <Feed user_id={props.match.params.userID} {...props} />)
      } />
      <Route path={"/auth"}>
        <Auth />
      </Route>
    </Switch>
  </Router>,
  document.getElementById('root')
);
