import React, { useEffect } from 'react'
import './feed.css'

import Back from './assets/back.png'

export default class CommunitySidebar extends React.Component {
    state = {
        curr_community_id: 0,
        your_communities: null,
        other_communities: null,
        join_community_id: null,
        join_community_name: null,
    }

    componentDidMount() {
        this.setState({
          curr_community_id: this.props.curr_community_id,
        })

        this.renderLeftSidebar();
    }

    componentWillUpdate(nextProps, nextState) {
      if(nextProps.your_communities !== this.props.your_communities || nextProps.other_communities != this.props.other_communities) {
        this.renderLeftSidebar();
      }
    }

    renderLeftSidebar() {
        const your_communities_divs = this.props.your_communities.map((item) => {
          return <div 
                  className=  {item.Community_id === this.state.curr_community_id ? "sidebar-community-info sidebar-curr_community cursor-pointer" 
                              : "sidebar-community-info cursor-pointer"}
                  onClick={() => {this.props.setCurrComm(item); this.setState({curr_community_id: item.Community_id}, () => this.renderLeftSidebar())}}
                  >
            <h3>{item.Name}</h3>
          </div>
        })

        console.log(this.props.other_communities)
        const other_communities_divs = this.props.other_communities.map((item) => {
          if(item.Is_User_In_Community === 0) {
            return <div className="sidebar-community-info">
            <h3>{item.Name}</h3>
            <p>{item.User_Count} Users</p>
            <button onClick={() => {this.props.setJoinCommOverlay(item.Community_Id); this.setState({join_community_name: item.Name, join_community_id: item.Community_Id})}}>join</button>
          </div>
          }
        })
    
        this.setState({
          your_communities: your_communities_divs,
          other_communities: other_communities_divs
        })
      }

      render() {
        let className = '';
        if (this.props.visible) {
            className += 'visible';
        } else if (!this.props.visible) {
            className += 'hidden';
        }

        if (this.props.isMobile) {
            className += ' mobile';
        }
        return(
                <div id="sidebar-div-comm" className = {className}>
                    <div id="sidebar-subdiv">
                      <div className="flex-row space-between">
                          <div id="back-div" onClick = {() => this.props.goBack()}>
                            <img src = {Back} className = {className}/>
                          </div>
                          <h3 >Your Communities</h3>
                          <button className="small-button" onClick={() => this.props.setNewCommOverlay(true)}>+</button>
                      </div>
                      <div id="sidebar-communities-scroll-div">
                        <div id="sidebar-communities-scroll-content">
                          {this.state.your_communities}
                        </div>
                      </div>
                    </div>
                    <div id="sidebar-subdiv">
                    <h3>Other Communities</h3>
                    <div id="sidebar-communities-scroll-div">
                      <div id="sidebar-communities-scroll-content">
                        {this.state.other_communities}
                      </div>
                    </div>
                    </div>
                </div>
          )}
}
