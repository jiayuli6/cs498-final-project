import React, {useState, useEffect} from "react";
import "./newpost.css"
import axios from "axios";

const NewPost = (props) => {
    const [isVisible, setVisible] = useState(props.visible);
    const [postContent, changePostContent] = useState("");
    const [communityName, setCommunityName] = useState("");
    const [publicName, setPublicName] = useState("");
    const [postTitle, changePostTitle] = useState("");

    useEffect(() => {
        setVisible(props.visible);
        changePostContent("");
        changePostTitle("")

        const config = {
            method: 'get',
            url: `https://cs498-final-project-0llama.herokuapp.com/api/community`,
            params: {
                userID: props.user_id,
                communityID: props.community_id,
            }
        }
        axios(config)
        .then((res) => {
            setCommunityName(res.data[0].Name);
            setPublicName(res.data[0].Public_Name);
        })
        .catch((err) => {
            console.log(err);
        })
    }, [props.user_id, props.community_id, props.visible])

    const postPost = () => { 
        const config = {
            method: 'post',
            url: `https://cs498-final-project-0llama.herokuapp.com/api/post`,
            params: {
                title: postTitle,
                publicName: publicName ? publicName : "anonymous",
                communityID: props.community_id,
                content: postContent,
                currentUser: props.user_id
            }
        }
        axios(config)
        .then((res) => {
            console.log(res);
            props.closeOverlay();
        })
        .catch((err) => {
            console.log(err);
        })
    }

    return(
        <div id="overlay-background" className={isVisible ? "visible" : "hidden"}>
            <div id="overlay-content" className="black-bottom-shadow">
                <div id="newpost-header">
                    <button onClick={() => props.closeOverlay()}>&#10005;</button>
                    <h1>New Post</h1>
                    <button onClick={() => {postPost(); props.closeOverlay()}}>post</button>
                </div>
                <div id="newpost-input-div">
                    <div className="flex-row">
                        <h4>Title: </h4>
                        <input type="text" value={postTitle} name="post_content" onChange={(e) => changePostTitle(e.target.value)}/>{' '}
                    </div>
                    <input type="text" value={postContent} className="post-text-field" name="post_content" onChange={(e) => changePostContent(e.target.value)}/>{' '}
                </div>
                <div id="newpost-user-info">
                    <h3>Posting in: {communityName}</h3>
                    <h4>Posting as: {publicName===null ? "anonymous" : publicName}</h4>
                </div>
            </div>
        </div>
    );
}

export default NewPost;