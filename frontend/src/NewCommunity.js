import React, {useEffect, useState} from "react";
import axios from "axios";
import { useHistory } from 'react-router-dom';

const NewCommunity = (props) => {
    const [isVisible, setVisible] = useState(props.visible);
    const [commName, setCommName] = useState("");
    const [password, setPassword] = useState("");
    const [userID, setUserID] = useState("");
    const history = useHistory();

    useEffect(() => {
        setVisible(props.visible);
        setUserID(props.user_id);
        setCommName("");
        setPassword("");
    }, [props.visible, props.user_id]);

    const createCommunity = () => {
        const axiosConnection = axios.create({
            baseURL: `https://cs498-final-project-0llama.herokuapp.com`
        });

        axiosConnection.post("/api/community", {
            communityName: commName,
            communityPassword: password,
            communityDescription: "new communiity"
        })
        .then((res) => {
            console.log(res); // maybe a message to successfully join

            setCommName("");
            setPassword("");

            const config = {
                method: 'post',
                url: `https://cs498-final-project-0llama.herokuapp.com/api/mycommunities`,
                params: {
                    userID: userID,
                    communityID: res.data[0].Community_Id,
                    publicName: null
                }
            }
            axios(config)
            .then((res) => {
                console.log(res.data);
                history.go(`0`)
            })
            .catch((err) => {
                console.log(err);
            })
        })
        .catch((err) => {
            console.log(err);
        })
    }

    return(
        <div id="overlay-background" className={isVisible ? "visible" : "hidden"}>
            <div id="overlay-content" className="black-bottom-shadow">
                <h1>Create a New Community</h1>
                <div className="flex-row">
                    <label>Name</label>
                    <input type="text" defaultValue={commName} onChange={(event) => setCommName(event.target.value)}/>
                </div>
                <div className="flex-row">
                    <label>Password</label>
                    <input type="text" defaultValue={password} onChange={(event) => setPassword(event.target.value)}/>
                </div>
                <div className="flex-row signin-action-div">
                    <button onClick={() => {setCommName(""); setPassword(""); props.closeOverlay();}}><p>Cancel</p></button>
                    <button onClick={() => {createCommunity(); props.closeOverlay()}}><p>Lets Go!</p></button>
                </div>
            </div>
        </div>    
    );
}

export default NewCommunity;