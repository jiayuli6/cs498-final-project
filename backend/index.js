const express = require("express");
const app = express();
const bodyParser = require("body-parser");
var mysql = require("mysql");
const e = require("express");
require('dotenv').config();

const db = mysql.createConnection({
    host: process.env.DB_HOST,
    user: process.env.DB_USER,
    password: process.env.DB_PASS,
    database: process.env.DB_NAME
});

db.connect(function(err) {
    if (err) {
        console.error('Error connecting to MYSQL: ' + err.stack);
        return;
    }
    console.log("Connected to MySQL...");
});

// Allow CORS so that backend and frontend could be put on different servers
var allowCrossDomain = function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "X-Requested-With, X-HTTP-Method-Override, Content-Type, Accept");
    res.header("Access-Control-Allow-Methods", "POST, GET, PUT, DELETE, OPTIONS");
    next();
};
app.use(allowCrossDomain);

app.use(express.json());
app.use(bodyParser.urlencoded({ extended: true }));

/* user apis */

// check if user exists
app.get("/api/user/:id", async (req, res) => {
    const userID = req.params.id;
    const sqlSelect = "SELECT * FROM USER WHERE User_Id=?";
    db.query(sqlSelect, userID, (err, result) => {
        if (err) {
            res.send(err);
        } else {    
            if (result.length > 0) {
                res.send('1');
            } else {
                res.send('0');
            }
        }
    });
});

// insert new signed up user
app.post("/api/user", (req, res) => {
    const userID = req.body.userID;
  
    const sqlInsert = "INSERT INTO USER (User_Id) VALUES (?);";
    db.query(sqlInsert, userID, (err, result) => {
        if (err) {
            res.send("Error add new user: " + err.stack);
        } else {
            res.send("Added new user: " + userID);
        }
    });
});

/* communities apis */

// get all community info
app.get("/api/communities/:id", async (req, res) => {
    const userID = req.params.id;
    const sqlSelect = 
        "SELECT c.Community_Id, c.Name, c.Description, c.User_Count, CASE WHEN tmp.Community_User_Id IS NULL THEN 0 ELSE 1 END AS Is_User_In_Community \
        FROM COMMUNITY c LEFT OUTER JOIN \
            (SELECT u.User_Id, cu.Community_User_Id, cu.Community_Id, cu.Public_Name \
            FROM USER u LEFT OUTER JOIN  COMMUNITY_USER cu ON u.User_Id=cu.User_Id \
            WHERE u.User_Id=?) tmp \
        ON c.Community_Id=tmp.Community_Id;";
    db.query(sqlSelect, userID, (err, result) => {
        if (err) {
            res.send(err);
        } else {    
            res.send(result);
        }
    });
});

/* community apis */

// get community info
app.get("/api/community/:id", async (req, res) => {
    const communityId = req.params.id;

    const sqlSelect = 
        "SELECT * FROM COMMUNITY WHERE Community_Id=?";
    db.query(sqlSelect, communityId, (err, result) => {
        if (err) {
            res.send("Error get community: " + err.stack);
        } else {
            res.send(result);
        }
    });
});

// get the community info according to community ID
app.get("/api/community", async (req, res) => {
    const userID = req.query.userID;
    const communityID = req.query.communityID;

    console.log("userID " + userID)
    console.log("communityID " + communityID)

    if(userID === undefined || communityID === undefined) {
        res.send("cannot find posts");
        return;
    }

    const sqlSelect = 
        "SELECT * \
        FROM COMMUNITY c JOIN COMMUNITY_USER cu ON c.Community_Id=cu.Community_Id \
        WHERE c.Community_Id = ? AND cu.User_Id=?";
    db.query(sqlSelect, [communityID, userID], (err, result) => {
        if (err) {
            res.send("Error get community: " + err.stack);
        } else {
            res.send(result);
        }
    });
});

// create a new community
app.post("/api/community", (req, res) => {
    const communityName = req.body.communityName;
    const communityPassword = req.body.communityPassword;
    const communityDescription = req.body.communityDescription;
  
    const sqlInsert = "INSERT INTO COMMUNITY (Name, Password, Description) VALUES (?, ?, ?);";
    const sqlSelect = "SELECT * FROM COMMUNITY WHERE Name = ?;";
    try {
        db.query(sqlInsert, [communityName, communityPassword, communityDescription], (err, result) => {
            if (err) {
                res.send("Error insert into community: " + err.stack);
            }
        });
    } finally {
        db.query(sqlSelect, communityName, (err, result) => {
            if (err) {
                res.send("Error return inserted community: " + err.stack);
            } else {
                res.send(result);
            }
        })
    }
});

// update the community description according to community name
app.put("/api/community", async (req, res) => {
    const communityName = req.body.communityName;
    const communityDescription = req.body.communityDescription;

    const sqlUpdate = "UPDATE COMMUNITY SET Description = ? WHERE Name = ?;";
    const sqlSelect = "SELECT * FROM COMMUNITY WHERE Name = ?;";
    try {
        db.query(sqlUpdate, [communityDescription, communityName], (err, result) => {
            if (err) {
                res.send("Error update community description: " + err.stack);
            }
        });
    } finally {
        db.query(sqlSelect, communityName, (err, result) => {
            if (err) {
                res.send("Error return updated community info: " + err.stack);
            } else {
                res.send(result);
            }
        });
    }
});


// delete community according to community name
app.delete("/api/community/:communityName", async (req, res) => {
    const communityName = req.params.communityName;

    const sqlDelete = "DELETE FROM COMMUNITY WHERE Name = ?;";
    db.query(sqlDelete, communityName, (err, result) => {
        if (err) {
            res.send("Error delete community: " + err.stack);
        } else {
            res.send(communityName);
        }
    });
});

/* mycommmunities apis */

//get all the communities' ID user has joined according to user ID
app.get("/api/mycommunities/:userID", async (req, res) => {
    const userID = req.params.userID;

    const sqlSelect = 
        `
        SELECT COMMUNITY.Community_id, COMMUNITY.Name FROM COMMUNITY 
        INNER JOIN COMMUNITY_USER 
        ON COMMUNITY.Community_Id = COMMUNITY_USER.Community_Id 
        WHERE COMMUNITY_USER.User_Id = ?;
        `;
    db.query(sqlSelect, userID, (err, result) => {
        if (err) {
            res.send("Error return all the communities user had joined: " + err.stack);
        } else {
            res.send(result);
        }
    })
});

// user join a new community
app.post("/api/mycommunities", async (req, res) => {
    const userID = req.query.userID;
    const communityID = req.query.communityID;
    const publicName = req.query.publicName;

    const sqlInsert = "INSERT INTO COMMUNITY_USER (User_Id, Community_Id, Public_Name) VALUES (?, ?, ?);";
    const sqlSelect = "SELECT * FROM COMMUNITY WHERE Community_Id = ?;";
    try {
        db.query(sqlInsert, [userID, communityID, publicName], (err, result) => {
            if (err) {
                res.send("Erro insert community user: " + err.stack);
                return;
            }
        });
    } finally {
        db.query(sqlSelect, communityID, (err, result) => {
            if (err) {
                res.send("Erro return community info that user belongs to: " + err.stack);
                return;
            } else {
                res.send(result);
                return;
            }
        });
    }
});

// user change public_name in their community
app.put("/api/mycommunities", async (req, res) => {
    const userID = req.query.userID;
    const communityID = req.query.communityID;
    const publicName = req.query.publicName;

    const sqlUpdate = 
        `UPDATE COMMUNITY_USER \
        SET Public_Name=? \
        WHERE User_Id=? AND Community_Id=?;`;
    const sqlSelect = "SELECT * FROM COMMUNITY_USER WHERE User_Id=? AND Community_Id=?;";
    try {
        db.query(sqlUpdate, [publicName, userID, communityID], (err, result) => {
            if (err) {
                res.send("Erro insert community user: " + err.stack);
                return;
            }
        });
    } finally {
        db.query(sqlSelect, [userID, communityID], (err, result) => {
            if (err) {
                res.send("Erro return community info that user belongs to: " + err.stack);
            } else {
                res.send(result);
            }
        });
    }
});

// user leave a community
app.delete("/api/mycommunities", async (req, res) => {
    const userID = req.query.userID;
    const communityID = req.query.communityID;

    const sqlDelete = "DELETE FROM COMMUNITY_USER WHERE User_Id = ? AND Community_Id = ?;";
    const sqlSelect = "SELECT Name FROM COMMUNITY WHERE Community_Id = ?;";
    try {
        db.query(sqlDelete, [userID, communityID], (err, result) => {
            if (err) {
                res.send("Error delete community user: " + err.stack);
            }
        });
    } finally {
        db.query(sqlSelect, communityID, (err, result) => {
            if (err) {
                res.send("Error return left community name: " + err.stack);
            } else {               
                res.send(result);
            }
        });
    }
});

/* posts apis */

// get all posts from the community according to community ID
app.get("/api/posts", (req, res) => {
    const userID = req.query.userID;
    const communityID = req.query.communityID;
    const sort = req.query.sort;

    console.log("userID " + userID)
    console.log("communityID " + communityID)

    if(userID === undefined || communityID === undefined) {
        res.send("cannot find posts");
        return;
    }

    const sqlSelect = sort==="date" ?
        "SELECT DISTINCT p.Post_Id, p.Title, p.Date_Created, p.Public_Name, p.Community_Id, p.Content, p.Like_Count, p.Comment_Count, CASE WHEN pl.Likes_Id IS NULL THEN 0 ELSE 1 END AS If_Curr_User_Liked \
        FROM POST p LEFT OUTER JOIN \
            (SELECT Likes_Id, Post_Id FROM POST_LIKE WHERE User_Id = ?) pl ON p.Post_Id=pl.Post_Id \
        WHERE p.Community_Id = ? \
        ORDER BY p.Date_Created DESC;"
        : 
        "SELECT DISTINCT p.Post_Id, p.Title, p.Date_Created, p.Public_Name, p.Community_Id, p.Content, p.Like_Count, p.Comment_Count, CASE WHEN pl.Likes_Id IS NULL THEN 0 ELSE 1 END AS If_Curr_User_Liked \
        FROM POST p LEFT OUTER JOIN \
            (SELECT Likes_Id, Post_Id FROM POST_LIKE WHERE User_Id = ?) pl ON p.Post_Id=pl.Post_Id \
        WHERE p.Community_Id = ? \
        ORDER BY p.Like_Count DESC;";
    db.query(sqlSelect, [userID, communityID], (err, result) => {
        if (err) {
            res.send("Error select posts from community: " + err.stack);
        } else {
            res.send(result);
        }
    });
});

/* post apis */

// get the post info according to post ID
app.get("/api/post/:postId", (req, res) => {
    const postID = req.params.postId;

    const sqlSelect = "SELECT * FROM POST JOIN (SELECT Community_Id, Name FROM COMMUNITY) tmp ON POST.Community_Id=tmp.Community_Id WHERE POST.Post_Id = ?;";
    db.query(sqlSelect, postID, (err, result) => {
        if (err) {
            res.send("Error return post info: " + err.stack);
        } else {
            console.log("didn't caught error");
            res.send(result);
        }
    });
});

// create a new post
app.post("/api/post", (req, res) => {
    const title = req.query.title;
    const publicName = req.query.publicName;
    const communityID = req.query.communityID;
    const content = req.query.content;
    const currentUser = req.query.currentUser;

    const sqlInsert = "INSERT INTO POST (Title, Public_Name, Community_Id, Content, Author_Id) VALUES (?, ?, ?, ?, ?);";
    const sqlSelect = "SELECT * FROM POST WHERE Author_Id = ? AND Community_Id = ? ORDER BY Date_Created DESC LIMIT 1;";
    try {
        db.query(sqlInsert, [title, publicName, communityID, content, currentUser], (err, result) => {
            if (err) {
                res.send("Error insert into post: " + err.stack);
                return;
            }
        });
    } finally {
        db.query(sqlSelect, [currentUser, communityID], (err, result) => {
            if (err) {
                res.send("Error return new post info: " + err.stack);
                return;
            } else {
                res.send(result);
                return;
            }
        });
    }  
});

/* like apis */

// get info if current user liked the post or not
app.get("/api/like", (req, res) => {
    const userID = req.query.userID;
    const postID = req.query.postID;

    const sqlSelect = "SELECT Likes_Id FROM POST_LIKE WHERE User_Id = ? AND Post_Id = ?;"
    db.query(sqlSelect, [userID, postID], (err, result) => {
        if (err) {
            res.send("Error select likes id: " + err.stack);
        } else {
            if (result != null) {
                console.log(result)
                if (result.length > 0) {
                    res.send(true);
                } else {
                    res.send(false);
                }
            } else {
                res.send("Check query, result is NULL");
            }
        }
    });
});

// like a post
app.post("/api/like", (req, res) => {
    const userID = req.query.userID;
    const postID = req.query.postID;

    const sqlInsert = "INSERT INTO POST_LIKE (User_Id, Post_Id) VALUES (?, ?);";
    const sqlSelect = "SELECT * FROM POST WHERE Post_Id = ?";
    try {
        db.query(sqlInsert, [userID, postID], (err, result) => {
            if (err) {
                res.send("Error like post: " + err.stack);
            }
        });
    } finally {
        db.query(sqlSelect, postID, (err, result) => {
            if (err) {
                res.send("Error return liked post info: " + err.stack);
            } else {
                res.send(result);
            }
        })
    }
});

// unlike a post
app.delete("/api/like", (req, res) => {
    const userID = req.query.userID;
    const postID = req.query.postID;

    const sqlDelete = "DELETE FROM POST_LIKE WHERE User_Id = ? AND Post_Id = ?;";
    const sqlSelect = "SELECT * FROM POST WHERE Post_Id = ?;";
    try {
        db.query(sqlDelete, [userID, postID], (err, result)=> {
            if (err) {
                res.send("Error unlike post: " + err.stack);
            }
        });
    } finally {
        db.query(sqlSelect, postID, (err, result)=> {
            if (err) {
                res.send("Error return unliked post info: " + err.stack);
            } else {
                res.send(result);
            }
        })
    }
})

/* comment */

// get list of comments for post according to post ID
app.get("/api/comment/:postID", (req, res)=> {
    const postID = req.params.postID;

    const sqlSelect = "SELECT * FROM COMMENT WHERE Post_Id = ?";
    db.query(sqlSelect, postID, (err, result)=> {
        if (err) {
            res.send("Error get list of comments: " + err.stack);
        } else {
            res.send(result);
        }
    })
})

// add new comment
app.post("/api/comment", (req, res) => {
    const postID = req.query.postID;
    const publicName = req.query.publicName;
    const content = req.query.content;

    console.log("HERE " + publicName)

    const sqlInsert = "INSERT INTO COMMENT(Post_Id, Public_Name, Content) VALUES (?, ?, ?);";
    const sqlSelect = "SELECT * FROM COMMENT WHERE Post_Id = ? ORDER BY Date_Created DESC LIMIT 1;";
    try {
        db.query(sqlInsert, [postID, publicName, content], (err, result) => {
            if (err) {
                res.send("Error add new comment: " + err.stack);
                return;
            }
        });
    } finally {
        db.query(sqlSelect, postID, (err, result) => {
            if (err) {
                res.send("Error return comment info: " + err.stack);
            } else {
                res.send(result);
            }
        })
    }
})

app.listen(process.env.PORT || 3001, () => {
    console.log("backend running ...");
});
    